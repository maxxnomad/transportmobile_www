import React from 'react';
import Modal from 'react-modal';
import styled from 'styled-components';
import { IconClose } from '../../elements/icons';

const customStyles = {
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.4)'
  },
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    border: 0,
    borderRadius: 6,
    padding: 0
  }
};

Modal.setAppElement('#root');

const ModalContentWrap = styled.div`
  padding: 10px;
  width: 294px;
`;

const TitleWrap = styled.div`
  display: grid;
  grid-template-columns: 1fr 20px;
  color: #333333;
  font-family: 'SF Pro Display';
  font-weight: 600;
  font-size: 20px;
  margin-bottom: 15px;
`;
const TitleModal = styled.div`
  padding: 6px;
`;

function ModalWrapper({ children, isOpen, handleCloseModal, title }) {
  return (
    <Modal
      isOpen={isOpen}
      // onAfterOpen={afterOpenModal}
      onRequestClose={handleCloseModal}
      style={customStyles}
      contentLabel='Setup Modal'
    >
      <ModalContentWrap>
        <TitleWrap>
          <TitleModal>{title}</TitleModal>
          <div onClick={handleCloseModal}>
            <IconClose />
          </div>
        </TitleWrap>
        {children}
      </ModalContentWrap>
    </Modal>
  );
}

export default ModalWrapper;
