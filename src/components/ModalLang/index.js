import React from 'react';
import { IconEng, IconRus } from '../../elements/icons';
import ModalWrapper from '../ModalWrapper';
import LineSetupModal from '../LineSetupModal';

function ModalLang(props) {
  const { isOpen, handleCloseModal, selectLang, changeLang } = props;

  return (
    <ModalWrapper
      isOpen={isOpen}
      handleCloseModal={handleCloseModal}
      title='Выбор языка'
    >
      <LineSetupModal
        title='Русский'
        checked={selectLang === 'ru'}
        handleCheck={() => changeLang('ru')}
      >
        <IconRus />
      </LineSetupModal>
      <LineSetupModal
        title='Английский'
        checked={selectLang === 'en'}
        handleCheck={() => changeLang('en')}
      >
        <IconEng />
      </LineSetupModal>
    </ModalWrapper>
  );
}

export default ModalLang;
