import React from 'react';
import {
  IconWheelchairSquare,
  IconNumberView,
  IconGlasses,
  IconVolumeSetup,
  IconTable
} from '../../elements/icons';
import ModalWrapper from '../ModalWrapper';
import LineSetupModal from '../LineSetupModal';

function ModalSetup(props) {
  const {
    isOpen,
    handleCloseModal,
    commonConfig: {
      colorType,
      labalVisible,
      numberVisible,
      tableHeaderVisible
    },
    changeColorType,
    changeLabalVisible,
    changeNumberVisible,
    changeTableHeaderVisible
  } = props;

  const clickColorType = () => {
    const type = colorType === 'night' ? 'day' : 'night';
    changeColorType(type);
  };

  return (
    <ModalWrapper
      isOpen={isOpen}
      handleCloseModal={handleCloseModal}
      title='Настройки'
    >
      <LineSetupModal
        title='Показывать наклейки'
        checked={labalVisible}
        handleCheck={() => changeLabalVisible(!labalVisible)}
      >
        <IconWheelchairSquare />
      </LineSetupModal>
      <LineSetupModal
        title='Показывать гос. номер'
        checked={numberVisible}
        handleCheck={() => changeNumberVisible(!numberVisible)}
      >
        <IconNumberView />
      </LineSetupModal>
      <LineSetupModal
        title='Версия для слабовидящих'
        checked={colorType === 'night'}
        handleCheck={() => clickColorType()}
      >
        <IconGlasses />
      </LineSetupModal>
      <LineSetupModal title='Озвучивание прогнозов'>
        <IconVolumeSetup />
      </LineSetupModal>
      <LineSetupModal
        title='Показывать заголовок таблицы'
        checked={tableHeaderVisible}
        handleCheck={() => changeTableHeaderVisible(!tableHeaderVisible)}
      >
        <IconTable />
      </LineSetupModal>
    </ModalWrapper>
  );
}

export default ModalSetup;
