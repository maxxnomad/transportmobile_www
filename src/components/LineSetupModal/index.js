import React, { useState } from 'react';
import styled from 'styled-components';
import CheckBox from '../../elements/CheckBox';

const LineWrap = styled.div`
  display: grid;
  grid-template-columns: 32px 27px 1fr;
  padding: 0 6px;
  align-items: center;
  margin-bottom: 19px;
`;

const LineText = styled.div`
  color: #333333;
  font-family: 'SF Pro Display';
  font-weight: 300;
  font-size: 16px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

function LineSetupModal(props) {
  const { children, title, checked, handleCheck } = props;
  const [isChek, handleIsCheck] = useState(false);

  return (
    <LineWrap onClick={handleCheck}>
      <CheckBox checked={checked} />
      {children}
      <LineText>{title}</LineText>
    </LineWrap>
  );
}

export default LineSetupModal;
