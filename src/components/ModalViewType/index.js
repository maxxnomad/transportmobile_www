import React from 'react';
import ModalWrapper from '../ModalWrapper';
import LineSetupModal from '../LineSetupModal';

function ModalViewType(props) {
  const { isOpen, handleCloseModal, visibleColumnsType, setViewType } = props;

  return (
    <ModalWrapper
      isOpen={isOpen}
      handleCloseModal={handleCloseModal}
      //   title='Настройки'
    >
      <LineSetupModal
        title='Следующий'
        checked={visibleColumnsType === 'next'}
        handleCheck={() => setViewType('next')}
      ></LineSetupModal>
      <LineSetupModal
        title='Текущая'
        checked={visibleColumnsType === 'current'}
        handleCheck={() => setViewType('current')}
      ></LineSetupModal>
      <LineSetupModal
        title='Направление'
        checked={visibleColumnsType === 'direction'}
        handleCheck={() => setViewType('direction')}
      ></LineSetupModal>
    </ModalWrapper>
  );
}

export default ModalViewType;
