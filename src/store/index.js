import { reducer as commonReducer } from './common';
import { reducer as pagesReducer } from './pages';
import { defaultSiteConfig } from '../_conf/default';

export default function rootReducer(state = defaultSiteConfig, action) {
  let result = {
    common: commonReducer(state.common, action),
    pages: pagesReducer(state.pages, action)
  };

  setTimeout(() => {
    localStorage.setItem('userConfig', JSON.stringify(result));
  }, 100);

  return result;
}
