export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';
export const CHANGE_COLOR_TYPE = 'CHANGE_COLOR_TYPE';

export const CHANGE_LABEL_VISIBLE = 'CHANGE_LABEL_VISIBLE';
export const CHANGE_NUMBER_VISIBLE = 'CHANGE_NUMBER_VISIBLE';
export const CHANGE_TABLE_HEADER_VISIBLE = 'CHANGE_TABLE_HEADER_VISIBLE';
export const ADD_FAVORITES = 'ADD_FAVORITES';
export const DELETE_FAVORITES = 'DELETE_FAVORITES';

export const addFavorites = id => {
  return {
    type: ADD_FAVORITES,
    payload: id
  };
};

export const deleteFavorites = id => {
  return {
    type: DELETE_FAVORITES,
    payload: id
  };
};

export const changeLabalVisible = visible => {
  return {
    type: CHANGE_LABEL_VISIBLE,
    payload: visible
  };
};
export const changeNumberVisible = visible => {
  return {
    type: CHANGE_NUMBER_VISIBLE,
    payload: visible
  };
};
export const changeTableHeaderVisible = visible => {
  return {
    type: CHANGE_TABLE_HEADER_VISIBLE,
    payload: visible
  };
};

export function changeLanguage(lang) {
  return {
    type: CHANGE_LANGUAGE,
    payload: lang
  };
}

export function changeColorType(type) {
  return {
    type: CHANGE_COLOR_TYPE,
    payload: type
  };
}

export function reducer(state = {}, action) {
  const { type, payload } = action;
  switch (type) {
    case ADD_FAVORITES:
      return { ...state, favorites: [...state.favorites, payload] };
    case DELETE_FAVORITES:
      return {
        ...state,
        favorites: state.favorites.filter(item => item !== payload)
      };

    case CHANGE_LABEL_VISIBLE:
      return { ...state, labalVisible: payload };
    case CHANGE_NUMBER_VISIBLE:
      return { ...state, numberVisible: payload };
    case CHANGE_TABLE_HEADER_VISIBLE:
      return { ...state, tableHeaderVisible: payload };

    case CHANGE_LANGUAGE:
      return { ...state, language: payload };

    case CHANGE_COLOR_TYPE:
      return {
        ...state,
        colorType: payload
      };

    default:
      return state;
  }
}
