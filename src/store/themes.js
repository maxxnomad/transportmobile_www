import { mergeWithNotUnionArrays } from '../utils';

export const getThemeConfig = (initState, config) => {
  const {
    common: { colorType, interfaceType }
  } = initState;

  const { themes } = config;
  const themeName = `${colorType}-${interfaceType}`;

  return mergeWithNotUnionArrays({}, themes.base, themes[themeName]);
};
