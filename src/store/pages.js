import { combineReducers } from 'redux';
import getStationRequest from '../api/getStation';
import getForecastRequest from '../api/getForecast';

const SET_GRID_HEADER_VISIBLE = 'SET_GRID_HEADER_VISIBLE';
const SET_STATION_INFO = 'SET_STATION_INFO';
const ERROR_STATION_INFO = 'ERROR_STATION_INFO';
const LOADING_STATION_INFO = 'LOADING_STATION_INFO';
const SET_STATION_FORECASTS = 'SET_STATION_FORECASTS';
const SET_VIEW_COLUMNS_TYPE = 'SET_VIEW_COLUMNS_TYPE';

export const setViewType = type => ({
  type: SET_VIEW_COLUMNS_TYPE,
  payload: type
});

export function setGridHeaderVisible(value) {
  return {
    type: SET_GRID_HEADER_VISIBLE,
    payload: value
  };
}

export const getStationInfo = id => dispatch => {
  dispatch({
    type: LOADING_STATION_INFO
  });
  getStationRequest(id)
    .then(({ data }) => {
      dispatch({
        type: SET_STATION_INFO,
        payload: data
      });
    })
    .catch(err =>
      dispatch({
        type: ERROR_STATION_INFO,
        payload: err
      })
    );
};

const groupById = arr => {
  const temp = arr.reduce((acc, elem) => {
    const date = elem.rid;
    if (!acc[date]) {
      acc[date] = [];
    }
    acc[date].push(elem);
    return acc;
  }, {});
  return Object.getOwnPropertyNames(temp).map(k => temp[k]);
};

export const getStationForecast = id => dispatch => {
  dispatch({
    type: LOADING_STATION_INFO
  });
  getForecastRequest(id)
    .then(({ data }) => {
      dispatch({
        type: SET_STATION_FORECASTS,
        payload: groupById(data)
      });
    })
    .catch(err =>
      dispatch({
        type: ERROR_STATION_INFO,
        payload: err
      })
    );
};

function stationForecasts(state = {}, action) {
  const { type, payload } = action;
  switch (type) {
    case SET_VIEW_COLUMNS_TYPE:
      return {
        ...state,
        grid: {
          ...state.grid,
          visibleColumnsType: payload
        }
      };
    case SET_GRID_HEADER_VISIBLE:
      return {
        ...state,
        grid: {
          header: {
            visible: payload
          }
        }
      };

    case SET_STATION_INFO:
      return {
        ...state,
        stationInfo: {
          ...state.stationInfo,
          loading: false,
          error: null,
          ...payload
        }
      };

    case SET_STATION_FORECASTS:
      return {
        ...state,
        stationInfo: {
          ...state.stationInfo,
          loading: false,
          error: null,
          forecasts: payload
        }
      };

    case ERROR_STATION_INFO:
      return {
        ...state,
        stationInfo: {
          ...state.stationInfo,
          loading: false,
          error: payload
        }
      };
    case LOADING_STATION_INFO:
      return {
        ...state,
        stationInfo: {
          ...state.stationInfo,
          loading: true
        }
      };

    default:
      return state;
  }
}

export const reducer = combineReducers({
  stationForecasts
});
