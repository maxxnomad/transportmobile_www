import React, { useState } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import {
  IconFavorite,
  IconWillShare,
  IconSetup,
  IconSchedule,
  IconFavoriteFill
} from '../../elements/icons';
import { FormattedMessage } from 'react-intl';
import ModalSetup from '../../components/ModalSetup';

const FooterWraper = styled.div`
  height: 159px;
  padding: 16px;
  background-color: ${props => props.theme.backgroundColor};
`;

const FooterItem = styled.div`
  padding-bottom: 12px;
  display: grid;
  grid-template-columns: 40px 1fr;
`;

const FooterText = styled.div`
  color: ${props => props.theme.menu.fontColor};
  font-family: 'SF Pro Display';
  font-weight: 500;
  font-size: ${props => props.theme.menu.fontSize};
  display: flex;
  align-items: center;
`;

function SiteFooter(props) {
  const {
    commonConfig,
    changeColorType,
    theme,
    changeLabalVisible,
    changeNumberVisible,
    changeTableHeaderVisible,
    deleteFavorites,
    addFavorites,
    stationId
  } = props;

  const [isModalSetup, handleIsModalSetup] = useState(false);
  const isFavorite = commonConfig.favorites.indexOf(stationId) >= 0;
  return (
    <ThemeProvider theme={theme}>
      <FooterWraper>
        <FooterItem>
          <IconSchedule color={theme.menu.fontColor} />
          <FooterText>
            <FormattedMessage
              id='pages.footer.menu.schedule'
              defaultMessage='Расписание'
              description='Расписание'
            />
          </FooterText>
        </FooterItem>
        <FooterItem
          onClick={() =>
            isFavorite ? deleteFavorites(stationId) : addFavorites(stationId)
          }
        >
          {!isFavorite ? (
            <IconFavorite color={theme.menu.fontColor} />
          ) : (
            <IconFavoriteFill />
          )}
          <FooterText>
            <FormattedMessage
              id={
                !isFavorite
                  ? 'pages.footer.menu.addToFavourite'
                  : 'pages.footer.menu.deleteToFavourite'
              }
              defaultMessage={
                !isFavorite ? 'Добавить в избранное' : 'Убрать из избранного'
              }
              description={
                !isFavorite ? 'Добавить в избранное' : 'Убрать из избранного'
              }
            />
          </FooterText>
        </FooterItem>
        <FooterItem onClick={() => handleIsModalSetup(true)}>
          <IconSetup color={theme.menu.fontColor} />
          <FooterText>
            <FormattedMessage
              id='pages.footer.menu.settings'
              defaultMessage='Настройки'
              description='Настройки'
            />
          </FooterText>
        </FooterItem>
        <FooterItem>
          <IconWillShare color={theme.menu.fontColor} />
          <FooterText>
            <FormattedMessage
              id='pages.footer.menu.share'
              defaultMessage='Поделиться'
              description='Поделиться'
            />
          </FooterText>
        </FooterItem>
        <ModalSetup
          isOpen={isModalSetup}
          handleCloseModal={() => handleIsModalSetup(false)}
          commonConfig={commonConfig}
          changeColorType={changeColorType}
          changeLabalVisible={changeLabalVisible}
          changeNumberVisible={changeNumberVisible}
          changeTableHeaderVisible={changeTableHeaderVisible}
        />
      </FooterWraper>
    </ThemeProvider>
  );
}

export default SiteFooter;
