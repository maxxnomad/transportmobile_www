import React, { useState } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { IconMenu, IconLang, IconStation } from '../../elements/icons';
import ModalLang from '../../components/ModalLang';

const HeaderWraper = styled.div`
  color: ${props => props.theme.fontColor};
  padding: 16px;
  height: 105px;
  background-color: ${props => props.theme.backgroundColor};
  display: grid;
  grid-template-rows: 1fr 1fr;
`;
const MenuTopWrap = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`;
const MenuButton = styled.div`
  color: red;
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

const MenuLang = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  font-family: 'SF Pro Display';
  font-weight: 600;
  font-size: ${props => props.theme.selectLang.fontSize};
  cursor: pointer;
`;

const MenuBottomWrap = styled.div`
  display: grid;
  grid-template-columns: 62px 4fr;
  .text {
    display: grid;
    justify-content: flex-start;
    font-family: 'SF Pro Display';
    .text-title {
      display: grid;
      font-size: ${props => props.theme.stationName.fontSize};
      justify-content: flex-start;
      font-weight: 700;
    }
    .text-body {
      display: grid;
      font-weight: 400;
      font-size: ${props => props.theme.stationDescription.fontSize};
      justify-content: flex-start;
    }
  }
`;

const SiteHeader = props => {
  const {
    lang,
    changeLanguage,
    configHeader: {
      stationInfo: { name, description, routeTypes }
    },
    theme
  } = props;

  const [isModalLang, handleIsModalLang] = useState(false);

  const clickLang = lng => {
    changeLanguage(lng);
    handleIsModalLang(false);
  };
  console.log(props);
  return (
    <ThemeProvider theme={theme}>
      <HeaderWraper>
        <MenuTopWrap>
          <MenuButton>
            <IconMenu />
          </MenuButton>
          <MenuLang>
            <IconLang />
            <span
              style={{ position: 'absolute', marginLeft: -21 }}
              onClick={() => handleIsModalLang(true)}
            >
              {lang.toUpperCase()}
            </span>
          </MenuLang>
        </MenuTopWrap>
        <MenuBottomWrap>
          <IconStation typeIcon='place' routeTypes={routeTypes} />
          <div className='text'>
            <div className='text-title'>{name}</div>
            <div className='text-body'>{description}</div>
          </div>
        </MenuBottomWrap>
        <ModalLang
          isOpen={isModalLang}
          handleCloseModal={() => handleIsModalLang(false)}
          selectLang={lang}
          changeLang={clickLang}
        />
      </HeaderWraper>
    </ThemeProvider>
  );
};
export default SiteHeader;
