//подразумевается, что приложение, перед запуском, склеивает итоговый конфиг из нескольких частей:
//1. "с сервера", получаем запросом к api: системные настройки из конфига серверной части(часовой пояс,...), настройки для сущностей из таблиц БД(цвет маршрута,...)
//2. "настройки по-умолчанию", лежат в файлах в папке _conf: настройки для gui-компонентов
//3. "персональные настройки", настройки, которые сохраняются в localStore процессе работы пользователя
//порядок склеивания настроек 1->3, то есть персональные настройки - самые приоритетные
var defaultSiteConfig = {
  //общие настройки
  common: {
    language: 'ru',
    interfaceType: 'normal', //'contrast'
    colorType: 'day', //'night'
    labalVisible: true,
    numberVisible: true,
    tableHeaderVisible: true,
    favorites: [],
    backend: {
      url:'http://5.188.114.86:8080',
      cityCode:"ryazan"
    },
    map: {
      centerLatLng: { lat: 53.243562, lng: 34.363407 },
      zoom: 12,
      tileSourceUrl: 'http://maps.ru:8080/getTile.php?z={z}&x={x}&y={y}&e=.png'
    }
  },
  //конфигурации всех страниц
  pages: {
    stationForecasts: {
      speakForecastsEnabled: false,
      title: {
        textAlign: 'left'
      },
      grid: {
        visibleColumns: ['routeName', 'nearest', 'next'], //'whereGo','lastStation'
        visibleColumnsType: 'next',
        header: {
          visible: true
        },
        row: {
          showTags: true,
          showGosNum: false
        }
      },
      stationInfo: {
        name: 'Дом художника',
        description: 'в сторону пл. Победы',
        routeTypes: [],
        loading: false,
        error: null,
        forecasts: []
      }
    }
  },
  themes: {
    magicNumber: 12345, //случ.число, помогает компонентам узнать, что темы изменились

    //базовая тема, другие темы, перед использованием, нужно склеивать с темой base
    base: {
      routeTypes: {
        А: { color: '#FF4238' },
        Т: { color: '#64B2BC' },
        М: { color: '#FDBF35' },
        Тр: { color: '#4BBA52' }
      },
      siteHeader: {
        backgroundColor: '#9D3ADC',
        fontColor: 'white',
        stationName: {
          fontSize: '20px'
        },
        stationDescription: {
          fontSize: '14px'
        },
        selectLang: {
          fontSize: '14px'
        }
      },
      StationForecastsTitle: {
        backgroundColor: 'white',
        borderColor: '#e4e4e4',
        title: {
          fontSize: '20px',
          fontColor: '#333333'
        },
        titleMenu: {
          fontSize: '14px',
          fontColor: '#b8b8b8'
        },
        menu: {
          fontSize: '14px',
          fontColor: '#333333'
        }
      },
      stationForecastsGrid: {
        backgroundColor: 'white',
        borderColor: '#e4e4e4',
        Forecast: {
          backgroundColor: 'rgba(241, 241, 241, 0.3)',
          borderColor: '#dddddd',
          time: {
            fontColor: '#333333',
            fontSize: '16px'
          },
          labelColor: '#333333',
          number: {
            fontColor: '#808080',
            fontSize: '12px'
          }
        },
        header: {
          fontSize: '12px',
          fontColor: 'black'
        },
        row: {
          fontSize: '16px',
          fontColor: 'black'
        }
      },
      siteFooter: {
        backgroundColor: 'white',
        menu: {
          fonColor: '#333333',
          fontSize: '16px',
          items: {
            schedule: true,
            favourite: true,
            settings: true,
            share: true
          }
        }
      },
      languageSelectionPopup: {
        backgroundColor: 'white',
        fontSize: '12px',
        fontColor: 'white'
      },
      settingsPopup: {
        backgroundColor: 'white',
        fontSize: '12px',
        fontColor: 'white'
      }
    },
    //здесь переопределяем только необходимые
    'night-normal': {
      routeTypes: {
        А: { color: '#F55A55' }
      },
      siteHeader: {
        backgroundColor: '#2F2F2F'
      },
      StationForecastsTitle: {
        backgroundColor: '#474747',
        borderColor: 'rgba(255, 255, 255, 0.2)',
        title: {
          fontColor: 'white'
        },
        menu: {
          fontColor: 'white'
        }
      },
      stationForecastsGrid: {
        backgroundColor: '#474747',
        borderColor: 'rgba(255, 255, 255, 0.2)',
        Forecast: {
          backgroundColor: 'rgba(255, 255, 255, 0.2)',
          borderColor: 'rgba(255, 255, 255, 0.5)',
          time: {
            fontColor: 'white'
          },
          labelColor: 'white',
          number: {
            fontColor: '#B5B5B5'
          }
        },
        header: {
          fontSize: '12px',
          fontColor: 'black'
        },
        row: {
          fontSize: '16px',
          fontColor: 'black'
        }
      },
      siteFooter: {
        backgroundColor: '#474747',
        menu: {
          fontColor: 'white'
        }
      }
    },
    'night-contrast': {
      stationForecastsGrid: {
        header: {
          fontSize: '18px',
          fontColor: 'white'
        },
        row: {
          fontSize: '24px',
          fontColor: 'white'
        }
      }
    },
    'day-normal': {},
    'day-contrast': {}
  }
};

export { defaultSiteConfig };
