import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import _ from 'lodash';
import { IntlProvider } from 'react-intl';
import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider, connect } from 'react-redux';
import messages_ru from './_translations/ru.json';
import messages_en from './_translations/en.json';
import { defaultSiteConfig } from './_conf/default';
import rootReducer from './store';
import { mergeWithNotUnionArrays, removeObjectsWithNull } from './utils';
import StationForecastsPage from './pages/StationForecastPage';
import axios from 'axios';

axios.defaults.baseURL = _.get(defaultSiteConfig,'common.backend.url','');
axios.defaults.params = {
  'city': _.get(defaultSiteConfig,'common.backend.cityCode',''),
};

const messages = {
  ru: messages_ru,
  en: messages_en
};

function paramsToObject(entries) {
  let result = {};
  for (let entry of entries) {
    const [key, value] = entry;
    result[key] = value;
  }
  return result;
}

function App() {
  var url = new URL(window.location.href);
  let queryParams = paramsToObject(url.searchParams);
  let siteConfigFromQuery = {
    common: {
      language: _.get(queryParams, 'lang')
    }
  };
  siteConfigFromQuery = removeObjectsWithNull(siteConfigFromQuery); //убираем ключи, для которых не нашлось параметров из query

  //конфиг из пользовательских настроек
  let userSiteConfig = JSON.parse(localStorage.getItem('userConfig') || '{}');

  //склеиваем итоговый конфиг в правильном порядке
  const siteConfig = mergeWithNotUnionArrays(
    {},
    defaultSiteConfig,
    userSiteConfig,
    siteConfigFromQuery
  );

  const store = createStore(
    rootReducer,
    siteConfig,
    compose(
      applyMiddleware(thunk),
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )
  );

  return (
    <div className='App'>
      <Provider store={store}>
        <Router>
          <Content />
        </Router>
      </Provider>
    </div>
  );
}

//если не вынести Content в отдельный компонент(вложенный в Router), то useQuery будет вылетать с ошибкой
let Content = connect(state => ({ language: state.common.language }))(props => {
  const lang = props.language;

  return (
    <IntlProvider locale={lang} messages={messages[lang]}>
      <Switch>
        <Route path='/station_forecasts'>
          <StationForecastsPage />
        </Route>
        <Route path='/'>
          <Redirect
            to={{
              pathname: '/station_forecasts',
              search: '?sid=76'
            }}
          />
        </Route>
      </Switch>
    </IntlProvider>
  );
});

export default App;
