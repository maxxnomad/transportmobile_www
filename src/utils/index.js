import _ from 'lodash';
import moment from 'moment';

// ######################### common #########################

function isNull(a) {
  return a === null || a === undefined;
}
function isNotNull(a) {
  return !isNull(a);
}
function isNumber(n) {
  return !isNaN(parseFloat(n)) && !isNaN(n - 0);
}
function getNumbersFromStr(str) {
  return str.match(/[0-9]+/g);
}
function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join('0') + num;
}
function dateToUtc(date) {
  var utcMinsOfs = date.getTimezoneOffset();
  return new Date(date.getTime() + utcMinsOfs * 60 * 1000);
}
function humanizeDuration(d) {
  if (d.years() > 0) return d.humanize(true);
  if (d.months() > 0) return d.months() + ' мес.';
  if (d.days() > 0) return d.days() + ' д.';
  if (d.hours() > 0) return d.hours() + ' ч.';
  if (d.minutes() > 0) return d.minutes() + ' м.';
  if (d.seconds() > 0) return d.seconds() + ' с.';
}

var returnNullFunc = function() {
  return null;
};
var isFunc = function(f) {
  return typeof f == 'function';
};
var tryCall = function(f) {
  if (isFunc(f)) return f.apply(null, Array.prototype.slice.call(arguments, 1));
  else return null;
};

// ######################### momentjs #########################
function getMomentFromTimestamp(time, timeZone) {
  return moment(time * 1000).utcOffset(timeZone);
}
function getMomentFromISO8601(time, timeZone) {
  //по-умолчанию moment распарсит время в timeZone из системного времени компьютера
  if (!timeZone) return moment(time);
  else if (time.charAt(time.length - 1) === 'Z')
    //если время указано в UTC
    return moment(time).utcOffset(timeZone);
  //то двигаем localtime, UTC не трогаем
  else return moment(time).utcOffset(timeZone, true); //иначе двигаем UTC, а localtime не трогаем
}
function getISO8601FromMoment(m, toUtc) {
  toUtc = isNull(toUtc) ? true : toUtc;
  if (toUtc)
    return (
      m
        .clone()
        .utcOffset(0)
        .format('YYYY-MM-DDTHH:mm:ss') + 'Z'
    );
  //сохраняем UTCtime, двигаем localtime
  else return m.format('YYYY-MM-DDTHH:mm:ss');
}
function getTimestampFromMoment(m, timeZone) {
  return m.utcOffset(timeZone, true).unix();
}
function getDateFromMoment(m) {
  return new Date(m.format('YYYY-MM-DD HH:mm:ss'));
}
function getMomentFromDate(d, timeZone) {
  if (!timeZone) return moment(d);
  else return moment(d).utcOffset(timeZone, true);
}
function isWeekendDay(m) {
  return m.day() === 6 || m.day() === 0;
}
// ######################### lodash #########################
//выполняем _.merge, но если встретим array, то подменяем его целиком, без объединения
function mergeWithNotUnionArrays() {
  var mainArguments = Array.prototype.slice.call(arguments);
  mainArguments.push(function(objValue, srcValue) {
    if (_.isArray(objValue) || _.isArray(srcValue)) {
      return srcValue;
    }
  });
  return _.mergeWith.apply(null, mainArguments);
}
function removeObjectsWithNull(obj) {
  return _(obj)
    .pickBy(_.isObject) // get only objects
    .mapValues(removeObjectsWithNull) // call only for values as objects
    .assign(_.omitBy(obj, _.isObject)) // save back result that is not object
    .omitBy(_.isNil) // remove null and undefined from object
    .value(); // get value
}

export { mergeWithNotUnionArrays, removeObjectsWithNull };
