import React, { useEffect, useRef } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  getStationInfo,
  getStationForecast,
  setViewType
} from '../../store/pages';
import {
  changeLanguage,
  changeColorType,
  changeLabalVisible,
  changeNumberVisible,
  changeTableHeaderVisible,
  addFavorites,
  deleteFavorites
} from '../../store/common';
import { getThemeConfig } from '../../store/themes';
import StationForecastsGrid from './StationForecastsGrid';
import SiteHeader from '../../widgets/SiteHeader/';
import SiteFooter from '../../widgets/SiteFooter/';
import StationForecastsTitle from './StationForecastsTitle';
import { defaultSiteConfig } from '../../_conf/default';

const StationForecastsPage = props => {
  const {
    getStationInfo,
    config,
    config: {
      stationInfo: { forecasts },
      grid: { visibleColumnsType }
    },
    changeLanguage,
    commonConfig: {
      language: lang = 'ru',
      tableHeaderVisible,
      labalVisible,
      numberVisible
    },
    commonConfig,
    themes,
    changeColorType,
    getStationForecast,
    setViewType,
    changeLabalVisible,
    changeNumberVisible,
    changeTableHeaderVisible,
    addFavorites,
    deleteFavorites
  } = props;

  let query = new URLSearchParams(props.location.search);
  const idStation = query.get('sid');

  useEffect(() => {
    idStation && getStationInfo(idStation);
  }, [getStationInfo, idStation]);

  const initialTimerId = useRef(false);

  useEffect(() => {
    if (idStation) {
      initialTimerId.current = setTimeout(async function setGetStatusLoop() {
        await getStationForecast(idStation);
        initialTimerId.current = setTimeout(setGetStatusLoop, 10000);
      }, 1000);
    } else {
      setTimeout(() => clearInterval(initialTimerId.current), 500);
    }
  }, [idStation, getStationForecast]);

  return (
    <>
      <SiteHeader
        configHeader={config}
        changeLanguage={changeLanguage}
        lang={lang}
        theme={themes.siteHeader}
      />
      <StationForecastsTitle
        theme={themes.StationForecastsTitle}
        visibleColumnsType={visibleColumnsType}
        setViewType={setViewType}
        tableHeaderVisible={tableHeaderVisible}
      />
      {forecasts &&
        forecasts.map((item, index) => {
          return (
            <StationForecastsGrid
              key={index}
              theme={themes.stationForecastsGrid}
              stationId={idStation}
              info={item}
              routeTypes={themes.routeTypes}
              visibleColumnsType={visibleColumnsType}
              labalVisible={labalVisible}
              numberVisible={numberVisible}
            />
          );
        })}

      <SiteFooter
        commonConfig={commonConfig}
        changeColorType={changeColorType}
        theme={themes.siteFooter}
        changeLabalVisible={changeLabalVisible}
        changeNumberVisible={changeNumberVisible}
        changeTableHeaderVisible={changeTableHeaderVisible}
        addFavorites={addFavorites}
        deleteFavorites={deleteFavorites}
        stationId={idStation}
      />
    </>
  );
};

const mapStateToProps = (state /*, ownProps*/) => ({
  config: state.pages.stationForecasts,
  themes: getThemeConfig(state, defaultSiteConfig),
  commonConfig: state.common
});

const mapDispatchToProps = dispatch => ({
  getStationInfo: id => dispatch(getStationInfo(id)),
  getStationForecast: id => dispatch(getStationForecast(id)),
  changeLanguage: lang => dispatch(changeLanguage(lang)),
  changeColorType: type => dispatch(changeColorType(type)),
  setViewType: type => dispatch(setViewType(type)),
  changeLabalVisible: visible => dispatch(changeLabalVisible(visible)),
  changeNumberVisible: visible => dispatch(changeNumberVisible(visible)),
  changeTableHeaderVisible: visible =>
    dispatch(changeTableHeaderVisible(visible)),
  addFavorites: id => dispatch(addFavorites(id)),
  deleteFavorites: id => dispatch(deleteFavorites(id))
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(StationForecastsPage)
);
