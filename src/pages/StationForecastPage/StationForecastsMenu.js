import React from "react";
import { FormattedMessage } from "react-intl";

export default function StationForecastsMenu() {
  return (
    <ul style={{ textAlign: "left" }}>
      <li>
        <FormattedMessage
          id="pages.stationForecasts.menu.schedule"
          defaultMessage="Расписание"
          description="Расписание"
        />
      </li>
      <li>
        <FormattedMessage
          id="pages.stationForecasts.menu.addToFavourite"
          defaultMessage="Добавить в избранное"
          description="Добавить в избранное"
        />
      </li>
      <li>
        <FormattedMessage
          id="pages.stationForecasts.menu.settings"
          defaultMessage="Настройки"
          description="Настройки"
        />
      </li>
      <li>
        <FormattedMessage
          id="pages.stationForecasts.menu.share"
          defaultMessage="Поделиться"
          description="Поделиться"
        />
      </li>
    </ul>
  );
}