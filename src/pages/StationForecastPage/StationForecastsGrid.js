import React from 'react';
import styled, { ThemeProvider } from 'styled-components';
import {
  IconArrowRight,
  IconWheelchair,
  IconConditioner
} from '../../elements/icons';
import { FormattedMessage } from 'react-intl';

const GridWraper = styled.div`
  border-bottom: 1px solid;
  border-color: ${props => props.theme.borderColor};
  padding: 10px 10px 11px 11px;
  background-color: ${props => props.theme.backgroundColor};
  display: grid;
  grid-template-columns: ${({ count }) => {
    if (count === 1) return '67px 1fr 2.2fr';
    if (count === 2) return '67px 1fr 1fr  1.2fr';
    if (count >= 3) return '67px 1fr 1fr 1fr 0.2fr';
    if (count === -1) return '67px 1fr 2fr 0.2fr';
  }};
  justify-content: flex-start;
  align-items: flex-start;
`;

const GridItem = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  margin: 6px 5px;
`;

const ArrowWrap = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  justify-content: flex-end;
  align-items: center;
`;

const Number = styled.div`
  width: 55px;
  height: 34px;
  background-color: ${props => props.color.color};
  border-radius: 3px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #ffffff;
  font-family: 'SF Pro Display';
  font-weight: 400;
  font-size: 16px;
`;

const TextNextStation = styled.div`
  font-family: 'SF Pro Display';
  font-weight: 400;
  color: ${props => props.theme.Forecast.time.fontColor};
  font-size: ${props => props.theme.Forecast.time.fontSize};
  margin: 6px;
  display: block;
  text-align: left;
`;

const Forecast = styled.div`
  width: 100%;
  background: ${props => props.theme.Forecast.backgroundColor};
  border: 1px solid;
  border-color: ${props => props.theme.Forecast.borderColor};
  border-radius: 4px;
  font-family: 'SF Pro Display';
  font-weight: 400;
  display: grid;
  justify-content: flex-start;
  align-items: flex-start;

  .text {
    color: ${props => props.theme.Forecast.time.fontColor};
    font-size: ${props => props.theme.Forecast.time.fontSize};
    margin: 6px;
    display: grid;
    justify-content: flex-start;
  }
  .text-number {
    font-size: ${props => props.theme.Forecast.number.fontSize};
    color: ${props => props.theme.Forecast.number.fontColor};
    margin: 0 0 6px 5px;
  }
  .icons {
    margin-left: 10px;
    margin-bottom: 6px;
    display: flex;
    svg + svg {
      margin-left: 8px;
    }
  }
`;

const renderItems = ({ info, theme, labalVisible, numberVisible }) => {
  return info.map((item, index) => {
    if (index < 3)
      return (
        <GridItem key={index}>
          <Forecast>
            <div className='text'>
              {`${Math.round(item.arrt / 60)} `}
              <FormattedMessage
                id='pages.title.menu.min'
                defaultMessage='мин'
                description='мин'
              />
            </div>
            {labalVisible && (
              <div className='icons'>
                {item.obj_tags.map(item => {
                  if (item === 2)
                    return (
                      <IconWheelchair
                        key={item}
                        color={theme.Forecast.labelColor}
                      />
                    );
                  if (item === 3) return <IconConditioner key={item} />;
                })}
              </div>
            )}
            {numberVisible && <div className='text-number'>{item.gos_num}</div>}
          </Forecast>
        </GridItem>
      );
  });
};

const renderItem = ({
  info,
  theme,
  labalVisible,
  numberVisible,
  visibleColumnsType
}) => {
  return (
    <>
      <GridItem>
        <Forecast>
          <div className='text'>
            {`${Math.round(info[0].arrt / 60)} `}
            <FormattedMessage
              id='pages.title.menu.min'
              defaultMessage='мин'
              description='мин'
            />
          </div>
          {labalVisible && (
            <div className='icons'>
              {info[0].obj_tags.map(item => {
                if (item === 2)
                  return (
                    <IconWheelchair
                      key={item}
                      color={theme.Forecast.labelColor}
                    />
                  );
                if (item === 3) return <IconConditioner key={item} />;
              })}
            </div>
          )}
          {numberVisible && (
            <div className='text-number'>{info[0].gos_num}</div>
          )}
        </Forecast>
      </GridItem>
      <TextNextStation>
        {visibleColumnsType === 'current' ? info[0].last : info[0].where}
      </TextNextStation>
    </>
  );
};

const renderColumns = stateItem => {
  switch (stateItem.visibleColumnsType) {
    case 'next':
      return renderItems(stateItem);
    case 'current':
      return renderItem(stateItem);
    case 'direction':
      return renderItem(stateItem);
    default:
  }
};

function StationForecastsGrid(props) {
  const {
    theme,
    info,
    routeTypes,
    visibleColumnsType,
    labalVisible,
    numberVisible
  } = props;

  const stateItem = {
    theme,
    info,
    visibleColumnsType,
    labalVisible,
    numberVisible
  };

  return (
    <ThemeProvider theme={theme}>
      <GridWraper count={visibleColumnsType === 'next' ? info.length : -1}>
        <GridItem>
          <Number color={routeTypes[info[0].rtype]}>{info[0].rnum}</Number>
        </GridItem>
        {renderColumns(stateItem)}
        <ArrowWrap>
          <IconArrowRight color={theme.Forecast.labelColor} />
        </ArrowWrap>
      </GridWraper>
    </ThemeProvider>
  );
}

export default StationForecastsGrid;
