import React, { useState } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { IconArrowDown } from '../../elements/icons';
import { FormattedMessage } from 'react-intl';
import ModalViewType from '../../components/ModalViewType';

const TitleWraper = styled.div`
  width: 100%;
  border-bottom: 1px solid;
  border-color: ${props => props.theme.borderColor};
  background-color: ${props => props.theme.backgroundColor};
`;

const TitleText = styled.div`
  color: ${props => props.theme.title.fontColor};
  font-family: 'SF Pro Display';
  font-weight: 600;
  font-size: ${props => props.theme.title.fontSize};
  padding-top: 16px;
  margin-bottom: 14px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding-left: 16px;
`;

const TitleTextMenu = styled.div`
  padding-left: 16px;
  display: grid;
  grid-template-columns: 67px 1fr 2.2fr;
  margin-bottom: 14px;
  div {
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;
    font-size: ${props => props.theme.titleMenu.fontSize};
    font-weight: 300;
    color: ${props => props.theme.titleMenu.fontColor};
  }
  .next {
    color: ${props => props.theme.menu.fontColor};
    /* margin-left: 24px; */
    margin-right: 5px;
    div {
      margin-left: 5px;
      margin-top: 3px;
    }
  }
`;

const typesView = {
  next: 'Следующий',
  current: 'Текущая',
  direction: 'Направление'
};

const typesViewId = {
  next: 'pages.title.menu.following',
  current: 'pages.title.menu.current',
  direction: 'pages.title.menu.direction'
};

const StationForecastsTitle = props => {
  const { theme, visibleColumnsType, setViewType, tableHeaderVisible } = props;
  const [isModalView, handleIsModalView] = useState(false);
  return (
    <ThemeProvider theme={theme}>
      <TitleWraper>
        <TitleText>
          <FormattedMessage
            id='pages.title.menu.routeInformation'
            defaultMessage='Информация по маршрутам'
            description='Информация по маршрутам'
          />
        </TitleText>
        {tableHeaderVisible && (
          <TitleTextMenu>
            <div>
              <FormattedMessage
                id='pages.title.menu.mashrut'
                defaultMessage='Машрут'
                description='Машрут'
              />
            </div>
            <div>
              <FormattedMessage
                id='pages.title.menu.forecast'
                defaultMessage='Прогноз'
                description='Прогноз'
              />
            </div>
            <div className='next' onClick={() => handleIsModalView(true)}>
              <FormattedMessage
                id={typesViewId[visibleColumnsType]}
                defaultMessage={typesView[visibleColumnsType]}
                description={typesView[visibleColumnsType]}
              />
              <div>
                <IconArrowDown color={theme.menu.fontColor} />
              </div>
            </div>
          </TitleTextMenu>
        )}
      </TitleWraper>
      <ModalViewType
        isOpen={isModalView}
        handleCloseModal={() => handleIsModalView(false)}
        visibleColumnsType={visibleColumnsType}
        setViewType={setViewType}
      />
    </ThemeProvider>
  );
};

export default StationForecastsTitle;
