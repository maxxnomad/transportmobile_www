import axios from 'axios';

export default idStation =>
  axios.get('/getStationForecastsWithTags.php', {
    params: { city: 'ryazan', sid: idStation }
  });
