import axios from 'axios';

export default idStation =>
  axios.get('/getStationInfo.php', {
    params: { city: 'ryazan', sid: idStation }
  });
