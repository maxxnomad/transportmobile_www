import React from 'react';
import styled from 'styled-components';
import { IconCheck } from '../icons';

const CheckBoxWrap = styled.div`
  width: 18px;
  height: 18px;
  border: 1px solid #dddddd;
  border-color: ${({ checked }) => (checked ? '#9D3ADC' : '#dddddd')};
  border-radius: 2px;
  background-color: ${({ checked }) => (checked ? '#9D3ADC' : '#ffffff')};
  display: flex;
  justify-content: center;
  align-items: center;
`;

function CheckBox({ checked }) {
  return (
    <CheckBoxWrap checked={checked}>{checked && <IconCheck />}</CheckBoxWrap>
  );
}

export default CheckBox;
