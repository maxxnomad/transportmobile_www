import React from 'react';

export default props => {
  const colorFill = props.color || '#ffffff';
  return (
    <svg
      width='46'
      height='20'
      viewBox='0 0 46 20'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <rect
        x='0.5'
        y='0.5'
        width='31'
        height='19'
        rx='3.5'
        stroke={colorFill}
      />
      <path
        d='M42.6547 12.4331C42.2789 12.7585 41.7211 12.7585 41.3453 12.4331L38.8314 10.2559C38.1315 9.64979 38.5602 8.5 39.4861 8.5L44.5139 8.5C45.4398 8.5 45.8685 9.64979 45.1686 10.2559L42.6547 12.4331Z'
        fill={colorFill}
      />
    </svg>
  );
};
