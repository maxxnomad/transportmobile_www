import IconMenu from './IconMenu';
import IconLang from './IconLang';
import IconStation from './IconStation';
import IconFavorite from './IconFavorite';
import IconSchedule from './IconSchedule';
import IconSetup from './IconSetup';
import IconWillShare from './IconWillShare';
import IconArrowDown from './IconArrowDown';
import IconArrowRight from './IconArrowRight';
import IconWheelchair from './IconWheelchair';
import IconConditioner from './IconConditioner';
import IconClose from './IconClose';
import IconCheck from './IconCheck';
import IconWheelchairSquare from './IconWheelchairSquare';
import IconNumberView from './IconNumberView';
import IconGlasses from './IconGlasses';
import IconVolumeSetup from './IconVolumeSetup';
import IconTable from './IconTable';
import IconEng from './IconEng';
import IconRus from './IconRus';
import IconFavoriteFill from './IconFavoriteFill';

export {
  IconMenu,
  IconLang,
  IconStation,
  IconFavorite,
  IconWillShare,
  IconSetup,
  IconSchedule,
  IconArrowDown,
  IconArrowRight,
  IconWheelchair,
  IconConditioner,
  IconClose,
  IconCheck,
  IconWheelchairSquare,
  IconNumberView,
  IconGlasses,
  IconVolumeSetup,
  IconTable,
  IconEng,
  IconRus,
  IconFavoriteFill
};
