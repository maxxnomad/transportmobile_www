import React from 'react';

export default props => {
  const colorFill = props.color || '#ffffff';
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='24'
      height='16'
      viewBox='0 0 24 16'
      fill='none'
    >
      <rect width='24' height='2' rx='1' fill={colorFill} />
      <rect y='7' width='24' height='2' rx='1' fill={colorFill} />
      <rect y='14' width='24' height='2' rx='1' fill={colorFill} />
    </svg>
  );
};
